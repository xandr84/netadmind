import std.stdio;

import vibe.data.json;
import vibe.core.file;
import vibe.core.core;
import vibe.core.log;
import std.zlib;

import defs;
import tcp;
import service;
import fileservice;
import netapp;
import utils;

int main()
{
    setLogLevel(LogLevel.debug_);
    
    auto logger = new HTMLLogger("actor.log.html");
	logger.minLogLevel = LogLevel.debugV;
	registerLogger(cast(shared)logger);

	auto clientService = new ClientCoreService(ClientType.ACTOR); 
	clientService.registerExtension(new FileSysExtension);

	NetApp.instance.init("actor.config", new TcpClient, clientService);
	return NetApp.instance.run();
}
