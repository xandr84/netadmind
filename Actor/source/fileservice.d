module fileservice;

import std.file;
import std.string;
import std.stream;
import std.zlib;
import std.datetime;
import core.time;
import vibe.core.file;
import vibe.core.core;

import defs;
import utils;
import filesys.defs;

class FileSysExtension : IExtension
{
    override @property string name() { return "FileSystem"; }
    override @property string ver() { return "1.0"; }
    override @property string info() { return "Файловый менеджер"; }
    
    override IService createService(uint id, uint[] clientIDs, IService parent, string config, ref string ver)
    {
        if(ver == this.ver) 
            return new ActorFileSysService(id, clientIDs, parent, config);  
        ver = this.ver;
        return null;
    }
}

class ActorFileSysService : FileSysService
{
    this(uint id, uint[] clientIDs, IService parent, string config)
    {
        super(id, parent);
        registerReceiverFunc(FileSystemMessage.QUERY_FOLDER_CONTENT, &processQueryFolderContent);
        registerReceiverFunc(FileSystemMessage.OPEN_FILE, &processOpenFile);
        registerReceiverFunc(FileSystemMessage.READ_FILE, &processReadFile);
        registerReceiverFunc(FileSystemMessage.PAUSE_TRASFER, &processTransferCommand);
        registerReceiverFunc(FileSystemMessage.RESUME_TRANSFER, &processTransferCommand);
        registerReceiverFunc(FileSystemMessage.STOP_TRANSFER, &processTransferCommand);
    }
    
    override void processMessage(ISender sender, HeaderPacket header, uint receiverIndex, ubyte[] data)
	{
        try 
        {
            super.processMessage(sender, header, receiverIndex, data);
        }
        catch(Exception e)
        {
            sendError(sender, header, 0, e.msg, FileSystemMessage.ERROR);
        }
	}

    void processQueryFolderContent(ISender sender, HeaderPacket header, ubyte[] data)
    {
        auto fcqp = UintStrPacket(data);
        FolderContentPacket fcp;

        SetErrorMode(SEM_FAILCRITICALERRORS);
        if(fcqp.text.length == 0)
        {
            enumDrives(fcp);
        }
        else if(exists(fcqp.text))
        {
            enumFolders(fcqp.text, fcp);
        }
        else
        {
            sendError(sender, header, FileSystemError.PATH_NOT_EXISTS,
                      format("Requested path not exists: '%s'", fcqp.text));
            return;
        }
        SetErrorMode(0);

        sendAnswer(sender, header, FileSystemMessage.FOLDER_CONTENT_INFO, 
                   cast(ubyte[])compress(fcp.serialize()));
    }

    void processOpenFile(ISender sender, HeaderPacket header, ubyte[] data)
    {
        auto p = UintStrPacket(data);
        
        m_stream.close();            
        m_stream = openFile(p.text, cast(vibe.core.file.FileMode)p.code);
        
        m_paused = false;
        m_stopped = false;
        
        FileInfoPacket fp;
        fp.name = p.text;
        fp.size = m_stream.size;
        
        sendAnswer(sender, header, FileSystemMessage.OPENED_FILE_INFO, fp.serialize());
    }
    
    void processTransferCommand(ISender sender, HeaderPacket header, ubyte[] data)
    {
        if(header.code == FileSystemMessage.PAUSE_TRASFER)
            m_paused = true;
        else if(header.code == FileSystemMessage.RESUME_TRANSFER)
            m_paused = false;
        else if(header.code == FileSystemMessage.STOP_TRANSFER)
            m_stopped = true;
    }

    void processReadFile(ISender sender, HeaderPacket header, ubyte[] data)
    {
        assert(m_stream.isOpen);
        
        
        auto p = FileDataRequestPacket(data);
        
        if(m_stream.size <= p.offset || p.size == 0)
        {
            sendAnswer(sender, header, FileSystemMessage.END_OF_FILE, null);
            return;
        }        
         
        ulong restSize = p.size + p.offset <= m_stream.size ? p.size : m_stream.size - p.offset;
        uint bufferSize = 64*1024;
        resizeBuffer(bufferSize);
        
        m_stream.seek(p.offset);
        
        while(restSize > 0)
        {
            while(m_paused && !m_stopped)
            {
                sleep(msecs(100));
            }
            
            if(m_stopped) break;
            
            uint size = cast(uint)(restSize > bufferSize ? bufferSize : restSize);
            m_stream.read(m_buffer[0..size]);
            
            SysTime startTime = Clock.currTime();
            sendAnswer(sender, header, FileSystemMessage.FILE_DATA, m_buffer[0..size]);
            
            auto d = Clock.currTime() - startTime;
            bufferSize = cast(uint)(500L * size / d.total!"msecs");
            bufferSize = bufferSize > 0x1000 ? bufferSize & 0xFFFFF000 : 0x1000;
            resizeBuffer(bufferSize);
            
            restSize -= size;
        }
        
        sendAnswer(sender, header, FileSystemMessage.END_OF_FILE, null);
    }

private:
    FileStream m_stream;
    bool m_paused, m_stopped;
    ubyte[] m_buffer;
    
    void resizeBuffer(uint size)
    {
        if(size < m_buffer.length) return;
        m_buffer = new ubyte[size];
    }
}