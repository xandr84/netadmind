# NetAdminD #

This is a simple analogue of Radmin and TeamViewer consisting of three components: 

* Actor is installed on the remote machine and connect to Proxy
* Proxy is the dedicated tcp server
* Admin is the component for managing the previous two

![Скриншот 2017-06-14 15.31.59.png](https://bitbucket.org/repo/88MEeL/images/370372164-%D0%A1%D0%BA%D1%80%D0%B8%D0%BD%D1%88%D0%BE%D1%82%202017-06-14%2015.31.59.png)