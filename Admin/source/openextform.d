module openextform;

import dgui.all;
import dgui.layout.layoutcontrol;

import std.array;

class OpenExtensionForm : Form
{
    this(string title)
    {
        super();
        
        this.text = title;
		this.size = Size(300, 200);
		this.startPosition = FormStartPosition.centerParent;
        this.formBorderStyle = FormBorderStyle.fixedDialog;
        this.maximizeBox = false;
        this.minimizeBox = false;
        
        m_textBox = new TextBox;
        m_textBox.parent = this;
        m_textBox.dock = DockStyle.fill;
        m_textBox.readOnly = true;
        m_textBox.multiline = true;        
    }
    
    override void show()
    {
        LayoutControl.show();
    }
    
    void appendLine(string line)
    {
        m_lines ~= line;
        updateLines();
    }
    
    void removeLastLine()
    {
        if(m_lines.length == 0) return;
        m_lines = m_lines[0..$-1];
        updateLines();
    }
    
private:
    TextBox m_textBox;
    string[] m_lines;
    
    void updateLines()
    {
        m_textBox.text = join(m_lines, "\r\n");
    }
}