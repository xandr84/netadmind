module configform;

import dgui.all;
import dgui.layout.panel;
import dgui.layout.gridpanel;
import dgui.layout.splitpanel;
import vibe.data.json;

import defs;
import utils;
import netapp;
import guidefs;
import service;

class ConfigForm : Form
{
    this(uint clientID, Json serviceConfigs)
    {
        super(); 

        m_clientID = clientID;        

        this.text = "Config client";
        this.size = Size(400, 300);
		this.startPosition = FormStartPosition.centerScreen;  
		this.icon = g_imgList16.images[ICON_GEAR];

        auto vpanel = new SplitPanel();
		vpanel.dock = DockStyle.fill;
		vpanel.splitPosition = 120;
		vpanel.splitOrientation = SplitOrientation.vertical; 
		vpanel.parent = this;
        vpanel.panel1.setPadding(4);
        vpanel.panel2.setPadding(4);

        m_servicesList = new ListBox;
        m_servicesList.dock = DockStyle.fill;
        m_servicesList.parent = vpanel.panel1;        

        auto buttons = new Panel;
        buttons.dock = DockStyle.bottom;
        buttons.height = 23;
        buttons.parent = vpanel.panel2;

        auto cancelButton = new Button;
        cancelButton.dock = DockStyle.right;
        cancelButton.text = "Отменить";
        cancelButton.width = 80;   
        cancelButton.parent = buttons;

        auto saveButton = new Button;
        saveButton.dock = DockStyle.right;
        saveButton.text = "Сохранить";
        saveButton.width = 80; 
        saveButton.parent = buttons;        

        m_editorPanel = new Panel;
        m_editorPanel.dock = DockStyle.fill; 
        m_editorPanel.parent = vpanel.panel2;
        m_editorPanel.setPadding(4);

        m_servicesInfo ~= ServiceInfo("Основной", null, null, 
            createTextConfigEditor(clientID, m_editorPanel, optValue(serviceConfigs, "", Json.emptyObject)));
        m_servicesInfo[0].editor.dock = DockStyle.fill;

        foreach(e; NetApp.instance.coreService.extensions)
        {
            auto si = ServiceInfo(e.name, e.ver, e.info);
            si.config = optValue(serviceConfigs, si.name, Json.emptyObject);
            si.editor = (cast(IGuiExtension)e).createConfigEditor(clientID, m_editorPanel, si.config);
            si.editor.visible = false;
            si.editor.dock = DockStyle.fill;
            m_servicesInfo ~= si;            
        }

        foreach(ref si; m_servicesInfo)
            m_servicesList.addItem(si.info);

        m_servicesList.itemChanged.attach(&this.onSelectServiceItem);
    }    

private:
    struct ServiceInfo
    {
        string name, ver, info;
        Control editor;
        Json config;
    }

    uint m_clientID;
    Panel m_editorPanel;
    ListBox m_servicesList;
    ServiceInfo[] m_servicesInfo;    

    void onSelectServiceItem(Control c, EventArgs e)
    {
        foreach(ref si; m_servicesInfo)
            si.editor.visible = false;

        m_servicesInfo[m_servicesList.selectedIndex].editor.visible = true;
        m_editorPanel.updateLayout();
    }
}
