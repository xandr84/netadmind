import std.stdio;

import vibe.data.json;
import vibe.core.file;
import vibe.core.core;
import vibe.core.log;
import std.functional;

import dgui.all;

import defs;
import tcp;
import netapp;
import service;
import mainform;
import utils;
import guidefs;
import filesys.service;

/*class GuiRegistryService : GuiService
{
    @property int imageIndex()
    {
        return ICON_REGISTRY;
    }

	Form createClientForm(uint clientID, Json clientConfig)
    {
        return null;
    }

    @property override ServiceDef definition()
	{
		return ServiceDef(EmbededServiceCode.REGISTRY, "Реестр");
	}
}*/

class AdminCoreService : ClientCoreService
{
    this()
    {
        super(ClientType.ADMIN);

        registerExtension(new GuiFileSysExtension);
        //registerService(new GuiRegistryService);        
    }

    void init(MainForm form)
    {
        m_form = form;
        registerReceiverFunc(CoreMessageType.CLIENTS_TREE_GROUP_DATA, &m_form.processClientsTreeGroupData);
        registerReceiverFunc(CoreMessageType.CLIENT_CONFIGS, &m_form.processClientConfigs);
        registerReceiverFunc(CoreMessageType.SERVICE_VERSION_RESULT, &m_form.processServiceVersion);
    }

protected:
    override void onConnect(uint clientID, ISender sender)
	{
        super.onConnect(clientID, sender);

        if(clientID != 0)
            m_form.onClientOnline(clientID);
    }

    override void onDisconnect(uint clientID, ISender sender)
	{
        super.onDisconnect(clientID, sender);

        if(clientID != 0)
            m_form.onClientOffline(clientID);
    }

    override void onClientReg(uint clientID, ISender sender)
    {
        super.onClientReg(clientID, sender);

        sender.sendMessage(makeHeader(0, selfID, CoreMessageType.QUERY_CLIENTS_TREE), makeUintPacket(0));
    }

private:
    MainForm m_form;
}

int main()
{
    setLogLevel(LogLevel.debug_);

	auto logger = new HTMLLogger("admin.log.html");
	logger.minLogLevel = LogLevel.debugV;
	registerLogger(cast(shared)logger);

    auto service = new AdminCoreService;    
	NetApp.instance.init("admin.config", new TcpClient, service);

    auto form = new MainForm;
    service.init(form);
	Application.run(form);

	return NetApp.instance.run();
}

