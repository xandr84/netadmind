module filesys.form;

import dgui.all;
import dgui.layout.layoutcontrol;
import dgui.layout.splitpanel;
import dgui.layout.panel;
import vibe.data.json;

import defs;
import netapp;
import utils;
import guidefs;
import filesys.defs;
import filesys.service;
import filesys.panel;

class FileSysForm : Form
{
	this(GuiFileSysService service)
	{
        super();

		m_service = service;

        this.text = "Файловый менеджер";
		this.size = Size(800, 600);
		this.startPosition = FormStartPosition.centerScreen;
		this.icon = g_imgList16.images[m_service.extension.imageIndex];
        
        auto bpanel = new Panel();
        bpanel.dock = DockStyle.bottom;
        bpanel.height = 40;
        bpanel.parent = this;
        
        m_progressBar = new ProgressBar();
        m_progressBar.dock = DockStyle.bottom;
        m_progressBar.height = 20;
        m_progressBar.parent = bpanel;
        
        auto lpanel = new Panel();
        lpanel.dock = DockStyle.fill;
        lpanel.height = 20;
        lpanel.parent = bpanel;
        
        m_speedLabel = new Label();
        m_speedLabel.dock = DockStyle.left;
        m_speedLabel.width = 80;
        m_speedLabel.text = "0 Kb/s";
        m_speedLabel.parent = lpanel;
        
        m_stopButton = new Button();
        m_stopButton.dock = DockStyle.right;
        m_stopButton.width = 80;
        m_stopButton.text = "Отмена";
        m_stopButton.parent = lpanel;
        
        m_pauseButton = new Button();
        m_pauseButton.dock = DockStyle.right;
        m_pauseButton.width = 80;
        m_pauseButton.text = "Пауза";
        m_pauseButton.parent = lpanel;
        
        m_operationLabel = new Label();
        m_operationLabel.dock = DockStyle.fill;
        m_operationLabel.text = "Ожидание";
        m_operationLabel.parent = lpanel;  
        
        auto vpanel = new SplitPanel();
		vpanel.dock = DockStyle.fill;
		vpanel.splitPosition = this.size.width/2;
		vpanel.splitOrientation = SplitOrientation.vertical;
		vpanel.parent = this;
        
        m_leftPanel = new FilePanel(this);
        m_leftPanel.dock = DockStyle.fill;
        m_leftPanel.parent = vpanel.panel1;
        m_leftPanel.init(true);

        m_rightPanel = new FilePanel(this);
        m_rightPanel.dock = DockStyle.fill;
        m_rightPanel.parent = vpanel.panel2; 
        m_rightPanel.init(false);  
	}
    
    @property GuiFileSysService fileService()
    {
        return m_service;
    }
    
    FilePanel getOtherPanel(FilePanel p)
    {
        return p is m_leftPanel ? m_rightPanel : m_leftPanel;
    }

	override void show()
    {
        LayoutControl.show();
    } 

private:
	GuiFileSysService m_service;
    FilePanel m_leftPanel, m_rightPanel; 
    ProgressBar m_progressBar;
    Label m_operationLabel, m_speedLabel;
    Button m_pauseButton, m_stopButton;
}
