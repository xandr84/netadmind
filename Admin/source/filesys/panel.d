module filesys.panel;

import dgui.all;
import dgui.layout.panel;
import vibe.inet.path;
import vibe.core.log;
import std.datetime;
import std.zlib;
import std.file;

import defs;
import utils;
import guidefs;
import service;
import filesys.defs;
import filesys.form;

class FilePanel : Panel
{
    this(FileSysForm form)
    {
        super();

        m_local = true;
        m_form = form;

        m_toolbar = new ToolBar();
        m_toolbar.imageList = g_imgList16;
        m_toolbar.parent = this;
        
        auto bpanel = new Panel();
        bpanel.dock = DockStyle.top;
        bpanel.height = 30;
        bpanel.parent = this;
        
        m_combo = new ComboBox();
        m_combo.width = 80;
        m_combo.dock = DockStyle.left;
        m_combo.dropDownStyle = DropDownStyles.dropdownList;
        m_combo.parent = bpanel;        
        m_combo.addItem("Локальный");
        m_combo.addItem("Удаленный");
        m_combo.itemChanged.attach(&this.onLocalRemoteChanged);

        m_title = new Label;
        m_title.height = 20;
        m_title.dock = DockStyle.fill;
        m_title.backColor = Color(153, 180, 209);
        m_title.parent = bpanel;        
        
        m_filesList = new ListView();
        m_filesList.dock = DockStyle.fill;
        m_filesList.parent = this;
        m_filesList.imageList = g_imgList16;
        m_filesList.viewStyle = ViewStyle.report;
        m_filesList.fullRowSelect = true;
        //m_filesList.contextMenu = m_clientContextMenu;
        m_filesList.addColumn("Имя", 170); 
        m_filesList.addColumn("Размер", 80, ColumnTextAlign.right); 
        m_filesList.addColumn("Дата", 110);
        m_filesList.doubleClick.attach(&onItemDoubleClicked);
        m_filesList.keyDown.attach(&onKeyDown);
    }
    
    @property string path() 
    { 
        return m_path.toNativeString(); 
    } 
    
    void init(bool local)
    {
        m_local = local;
        m_combo.selectedIndex(m_local ? 0 : 1);
        onLocalRemoteChanged(null, null);        
    }    

    void loadItems(FolderContentPacket content)
    {
        m_filesList.clear();
        m_filesList.addItem("..", ICON_BACK);

        foreach(f; content.folders)
        {
            auto item = m_filesList.addItem(f.name.dup);

            switch(cast(uint)f.type)
            {
                case FolderType.DRIVE_REMOVABLE:
                    item.imageIndex = ICON_HARDDISK;
                    item.addSubItem("<СЪЕМНЫЙ>");
                    break;

                case FolderType.DRIVE_FIXED:
                    item.imageIndex = ICON_HARDDISK;
                    item.addSubItem("<ДИСК>");
                    break;

                case FolderType.DRIVE_REMOTE:
                    item.imageIndex = ICON_NETDRIVE;
                    item.addSubItem("<СЕТЬ>");
                    break;

                case FolderType.DRIVE_CDROM:
                    item.imageIndex = ICON_CD;
                    item.addSubItem("<CD/DVD>");
                    break;

                case FolderType.DRIVE_RAMDISK:
                    item.imageIndex = ICON_HARDDISK;
                    item.addSubItem("<RAM>");
                    break;

                case FolderType.DIRECTORY:
                    item.imageIndex = ICON_FOLDER;
                    item.addSubItem("<ПАПКА>");
                    break;

                case FolderType.SYMLINK:
                    item.imageIndex = ICON_FOLDER;
                    item.addSubItem("<ССЫЛКА>");
                    break;

                default:
                    item.imageIndex = ICON_UNKNOWN;
                    item.addSubItem("<???>");
                    break;
            }

            if(f.type == FolderType.DIRECTORY || f.type == FolderType.SYMLINK)
            {
                item.addSubItem(timeToSqlStr(SysTime(f.timeLastModified)));
            }
            else
            {
                double fr = f.free/1024.0/1024/1024;
                double cp = f.capacity/1024.0/1024/1024;
                item.addSubItem(format("%.1fГб из %.1fГб", fr, cp));
            }
        }

        foreach(f; content.files)
        {
            auto item = m_filesList.addItem(f.name.dup, ICON_FILE);
            item.addSubItem(ulongToSpacedStr(f.size));
            item.addSubItem(timeToSqlStr(SysTime(f.timeLastModified)));
        }

        m_foldersAmount = content.folders.length;
        m_filesList.redraw();
    }
    
    void processFolderContentInfo(ISender sender, HeaderPacket header, ubyte[] data)
    {
		ubyte[] u = cast(ubyte[])uncompress(data);
        auto p = FolderContentPacket(u);
        loadItems(p);
    }

protected:
    bool m_local;
    Label m_title;
    ComboBox m_combo;
    ToolBar m_toolbar;
    ListView m_filesList;
    
    Path m_path;
    FileSysForm m_form;  
    uint m_foldersAmount;

    void onBackFolder()
    {
        m_path = m_path.parentPath;
        m_title.text = path;
        
        if(m_local) fillLocalFolder(this.path);
        else queryRemoteFolder(this.path); 
    }

    void onFolderEnter(string name)
    {
        if(m_path.empty)
            m_path = Path(name);
        else m_path ~= name;  

        m_title.text = path;
        
        if(m_local) fillLocalFolder(this.path);
        else queryRemoteFolder(this.path); 
    }

    void onFileOpen(string filePath)
    {
        //m_form.fileTransPanel.addItem(new FileTransCommand(FileTransCommand.Type.OPEN, filePath));
    }
    
    void onCopy(string fromPath, FilePanel toPanel, bool withMove)
    {
        if(m_local)
        {
            //if(toPanel.m_local)
            //    localCopyFile(fromPath, toPanel.path, withMove);            
        }
        else
        {
            //if(toPanel.m_local)
                
        }
    }
    
    void onDelete(string filePath)
    {
    }

private:
    void selectedItemEnter()
    {
        auto item = m_filesList.selectedItem;

        if(item.index == 0)
        {
            if(!m_path.empty)
                onBackFolder();
        }
        else if(item.index <= m_foldersAmount)
        {
            onFolderEnter(item.text);
        }
        else onFileOpen(item.text);
    }
    
    void selectedItemCopy(bool withMove)
    {
        auto item = m_filesList.selectedItem;
        
        if(item.index == 0) return;
        
        onCopy(path ~ '\\' ~ item.text, m_form.getOtherPanel(this), withMove);
    }
    
    void selectedItemDelete()
    {
        auto item = m_filesList.selectedItem;
        
        if(item.index == 0) return;
        
        onDelete(path ~ '\\' ~ item.text);
    }

    void onItemDoubleClicked(Control control, MouseEventArgs args)
    {
        selectedItemEnter();
    }
    
    void onLocalRemoteChanged(Control control, EventArgs args)
    {
        m_local = m_combo.selectedIndex == 0;
        m_path
        
        if(m_local)
        {
            fillLocalFolder("");
        }
        else
        {
            m_form.fileService.registerReceiverFunc(FileSystemMessage.FOLDER_CONTENT_INFO, &processFolderContentInfo);
            queryRemoteFolder("");
        }
    }

    void onKeyDown(Control control, KeyEventArgs args)
    {
        switch(args.keyCode & Keys.keyCode)
        {
        case Keys.enter:
            selectedItemEnter();
            args.handled = true;
            break;

        case Keys.back:
            if(!m_path.empty)
                onBackFolder();
            args.handled = true;
            break;
            
        case Keys.f5:
            selectedItemCopy(false);
            args.handled = true;
            break;
            
        case Keys.f6:
            selectedItemCopy(true);
            args.handled = true;
            break;
            
        case Keys.f8:
            selectedItemDelete();
            args.handled = true;
            break;

        default:
            break;
        }
    }
    
    void fillLocalFolder(string path)
	{
		FolderContentPacket fcp;

        SetErrorMode(SEM_FAILCRITICALERRORS);
        scope(exit) SetErrorMode(0);
        
		if(path.length == 0)
			m_form.fileService.enumDrives(fcp);
		else if(exists(path))
            m_form.fileService.enumFolders(path, fcp);
        else 
        {
            onBackFolder();
            return;
        }

		loadItems(fcp);
	}
    
    void queryRemoteFolder(string path)
    {
        UintStrPacket qp;
        qp.text = path;

        sendMessageNonBlocking(
            makeHeader(m_form.fileService.clientIDs, m_form.fileService.fullID, 
                FileSystemMessage.QUERY_FOLDER_CONTENT), qp.serialize()
        );
    }
}