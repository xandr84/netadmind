module filesys.service;

import dgui.all;
import vibe.data.json;

import defs;
import netapp;
import utils;
import guidefs;
import service;
import filesys.defs;
import filesys.form;

class GuiFileSysExtension : IGuiExtension
{
    override @property string name() { return "FileSystem"; }
    override @property string ver() { return "1.0"; }
    override @property string info() { return "Файловый менеджер"; }  
    
	override @property int imageIndex()
	{
		return ICON_FILESYS;
	}
    
    override IService createService(uint id, uint[] clientIDs, IService parent, string config, ref string ver)
    {
        if(ver == this.ver) 
            return new GuiFileSysService(id, clientIDs, parent, config, this);  
        ver = this.ver;
        return null;
    }

    override Control createConfigEditor(uint clientID, Control parent, Json config)
	{
		return createTextConfigEditor(clientID, parent, config);
	}

    override bool trySaveConfig(Control control, ref Json config, ref string error)
    {
		return trySaveTextConfig(control, config, error);
    }
}

class GuiFileSysService : FileSysService, IGuiService
{
    this(uint id, uint[] clientIDs, IService parent, string config, GuiFileSysExtension ext)
    {
        super(id, parent);
        m_config = parseJsonString(config);
        m_ext = ext;
        m_clientIDs = clientIDs;
    }
    
    @property Json config()
    {
        return m_config;
    }
    
    @property GuiFileSysExtension extension()
    {
        return m_ext;
    }
    
    @property uint[] clientIDs()
    {
        return m_clientIDs;
    }
    
    override Form createClientForm()
	{
		m_form = new FileSysForm(this);        
        return m_form;
	}
    
private:
    Json m_config;
    uint[] m_clientIDs;
    GuiFileSysExtension m_ext;
    FileSysForm m_form;
}
