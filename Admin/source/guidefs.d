module guidefs;

import dgui.all;
import dgui.layout.layoutcontrol;
import vibe.data.json;
import vibe.core.core;

import defs;
import service;
import netapp;

enum 
{
	ICI_FIRST = 100,
	ICI_LAST = 119,

	ICON_APP = 0,
	ICON_FOLDER = 1,
	ICON_ABOUT = 2,
	ICON_EXIT = 3,
	ICON_GEAR = 4,
	ICON_DEL = 5,
    ICON_STATION_ON = 6,
    ICON_STATION_OFF = 7,
    ICON_FILESYS = 8,
    ICON_CD = 9,
    ICON_FILE = 10,
    ICON_NETDRIVE = 11,
    ICON_UNKNOWN = 12,
    ICON_HARDDISK = 13,
	ICON_BACK = 14,
    ICON_REGISTRY = 15,
    ICON_ADMIN_ON = 16,
    ICON_ADMIN_OFF = 17,
    ICON_SERVER_ON = 18,
    ICON_SERVER_OFF = 19
}

enum {
    LVM_UPDATE = LVM_FIRST + 42
}

ImageList g_imgList16, g_imgList32;

static this()
{
    g_imgList16 = new ImageList();
    g_imgList16.size = Size(16, 16);
    g_imgList32 = new ImageList();
    g_imgList32.size = Size(32, 32);

    for(ushort i = ICI_FIRST; i <= ICI_LAST; i++)
    {
        g_imgList16.addImage(Application.resources.getIcon(i));
        g_imgList32.addImage(Application.resources.getIcon(i));
    }
}

interface IGuiService : IService
{
    Form createClientForm();
}

interface IGuiExtension : IExtension
{
    @property int imageIndex();    
    Control createConfigEditor(uint clientID, Control parent, Json config);
    bool trySaveConfig(Control control, ref Json config, ref string error);
}

Control createTextConfigEditor(uint clientID, Control parent, Json config)
{
    auto textBox = new TextBox;
    textBox.multiline = true;
    textBox.readOnly = false;
    textBox.parent = parent;
    textBox.text = config.toPrettyString();
    return textBox;
}

bool trySaveTextConfig(Control control, ref Json config, ref string error)
{
    try 
    {
        config = parseJsonString(control.text);
    }
    catch(Exception e) 
    {
        error = e.toString();
        return false;
    }

    return true;
}

void sendMessageNonBlocking(HeaderPacket header, ubyte[] data)
{
    runTask({
        NetApp.instance.sender.sendMessage(header, data);
    });
}
