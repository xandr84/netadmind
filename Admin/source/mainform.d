module mainform;

import dgui.all;
import dgui.layout.layoutcontrol;
import dgui.layout.splitpanel;
import vibe.core.log;
import vibe.core.core;
import vibe.data.json;
import std.format;

import defs;
import netapp;
import utils;
import service;
import guidefs;
import configform;
import openextform;

class MainForm: Form
{
	this()
	{
        super();          

		this.text = "NetAdmin";
		this.size = Size(800, 600);
		this.startPosition = FormStartPosition.centerScreen;
		this.icon = g_imgList16.images[ICON_APP];		

		m_mainMenu = new MenuBar();
		m_mainMenu.imageList = g_imgList16;

		MenuItem clientMenu = m_mainMenu.addItem("Клиент");
            m_servicesMenu = clientMenu.addItem("Запустить", false); 
            m_deleteMenu = clientMenu.addItem("Удалить", ICON_DEL, false);
            m_propertyMenu = clientMenu.addItem("Свойства...", ICON_GEAR, false); 
            m_propertyMenu.click.attach(&onProperty);
            clientMenu.addSeparator();
            clientMenu.addItem("Параметры...", ICON_GEAR);
            clientMenu.addSeparator();
            clientMenu.addItem("Выход", ICON_EXIT).click.attach(&onExit);       

		MenuItem viewMenu = m_mainMenu.addItem("Вид");

        MenuItem helpMenu = m_mainMenu.addItem("Справка");
            helpMenu.addItem("О программе...", ICON_ABOUT).click.attach(&onAbout);

		this.menu = m_mainMenu; 

        m_clientContextMenu = new ContextMenu;
		m_clientContextMenu.imageList = g_imgList16;
        m_clientContextMenu.addItem("Свойства...", ICON_GEAR).click.attach(&onProperty); 
        m_clientContextMenu.addItem("Удалить", ICON_DEL);
        m_clientContextMenu.addSeparator();

        foreach(s; NetApp.instance.coreService.extensions)
        {
            auto g = cast(IGuiExtension)s;

            MenuItem item = m_servicesMenu.addItem(s.info, g.imageIndex);
            item.click.attach(&onService);
            m_menuToService[item] = s;

            item = m_clientContextMenu.addItem(s.info, g.imageIndex);
            item.click.attach(&onService);
            m_menuToService[item] = s;
        }

		auto tbrToolbar = new ToolBar();
		tbrToolbar.imageList = g_imgList32;
		tbrToolbar.parent = this;

		tbrToolbar.addButton(ICON_DEL);
		tbrToolbar.addSeparator();
		tbrToolbar.addButton(ICON_GEAR);
		tbrToolbar.addButton(ICON_ABOUT).click.attach(&onAbout);			

		auto vpanel = new SplitPanel();
		vpanel.dock = DockStyle.fill;
		vpanel.splitPosition = 200;
		vpanel.splitOrientation = SplitOrientation.vertical; 
		vpanel.parent = this;

		m_groupTree = new TreeView();
		m_groupTree.dock = DockStyle.fill;
		m_groupTree.parent = vpanel.panel1;
		m_groupTree.imageList = g_imgList16;
        m_groupTree.selectedNodeChanged.attach(&onTreeItemSelected);
        m_groups[0] = GroupInfo();
		m_groups[0].node = m_groupTree.addNode("\\", ICON_FOLDER, NodeInsertMode.tail);
        m_nodes[m_groups[0].node] = 0;
        m_selectedGroupID = 0;

		m_clientList = new ListView();
		m_clientList.dock = DockStyle.fill;
		m_clientList.parent = vpanel.panel2;
        m_clientList.imageList = g_imgList16;
        m_clientList.viewStyle = ViewStyle.report;
        m_clientList.fullRowSelect = true;
        m_clientList.contextMenu = m_clientContextMenu;
        m_clientList.addColumn("Имя", 120); 
        m_clientList.addColumn("ПК", 100); 
        m_clientList.addColumn("Домен", 80); 
        m_clientList.addColumn("IP", 100); 
        m_clientList.addColumn("ОС", 60); 
        m_clientList.addColumn("Подключался", 100);
        m_clientList.itemChanged.attach(&onListItemSelected);
        m_selectedClientID = uint.max;
	}

	override void show()
    {
        LayoutControl.show();
    }

    void saveServiceConfigs(uint clientID, Json[uint] configs)
    {
        
    }

    void onClientOnline(uint clientID)
    {
        ClientInfo* c = clientID in m_clients;
		assert(c);
		c.data.online = true;

		if(m_groups[c.parentID].node == m_groupTree.selectedNode)
		{
			c.item.imageIndex = ICON_STATION_ON;
			m_clientList.redraw();
		}
    }

    void onClientOffline(uint clientID)
    {
        ClientInfo* c = clientID in m_clients;
		assert(c);
		c.data.online = false;

		if(m_groups[c.parentID].node == m_groupTree.selectedNode)
		{
			c.item.imageIndex = ICON_STATION_OFF;
			m_clientList.redraw();
		}
    }    

    void processClientsTreeGroupData(ISender sender, HeaderPacket header, ubyte[] data)
    {
        auto p = ClientsTreeGroupPacket(data);
        GroupInfo *root = p.rootID in m_groups;		

        if(!root)
        {
            logError("Received unknown tree group with id %d", p.rootID);
            return;
        }

        foreach(ref g; p.groups)
        {
			g.name = g.name.dup;
            m_groups[g.id] = GroupInfo(g, p.rootID);
            root.subgroups ~= g.id;
			logDebug("Receive group %d '%s'", g.id, g.name);
        }

        foreach(ref c; p.clients)
        {
			c.name = c.name.dup;
			c.lastVisit = c.lastVisit.dup;
			c.info.name = c.info.name.dup;
			c.info.domain = c.info.domain.dup;
			c.info.os = c.info.os.dup;
			c.info.ip = c.info.ip.dup;
            m_clients[c.id] = ClientInfo(c, p.rootID);
            root.clients ~= c.id;
			logDebug("Receive client id=%d, type=%d, name=%s, comp=%s, domain=%s", 
					 c.id, cast(uint)c.info.type, c.name, c.info.name, c.info.domain);
        }

        root.loaded = true;		

        updateTreeNode(root);
        updateList(root);
    }

    void processClientConfigs(ISender sender, HeaderPacket header, ubyte[] data)
    {
        auto p = ClientConfigsPacket(data);
        m_selectedConfigs = p.configs;
        Json configs = parseJsonString(p.configs);
        
        if(m_selectedExtension)
        {
            assert(m_waitdlg);
            m_waitdlg.removeLastLine();
            m_waitdlg.appendLine("Параметры клиента получены");
            m_waitdlg.appendLine("Согласование версий расширений");
            
            ServiceVersionPacket sp;
            sp.name = m_selectedExtension.name;
            sp.ver = m_selectedExtension.ver;
            sp.config = optValue(configs, m_selectedExtension.name).toString();
            
            sendMessageNonBlocking(
                makeHeader(p.clientID, NetApp.instance.coreService.selfID, CoreMessageType.NEGOTIATE_SERVICE_VERSION),
                sp.serialize()
            );
        }
        else
        {
            auto form = new ConfigForm(p.clientID, configs);
            if(form)
            {
                form.text = "Свойства - " ~ getClientPathName(p.clientID);
                form.show();
            }
        }
    }

    void onService(MenuItem sender, EventArgs e)
    {
        if(m_selectedClientID == uint.max || sender !in m_menuToService) return;

        m_selectedExtension = cast(IGuiExtension)m_menuToService[sender];
        
        m_waitdlg = new OpenExtensionForm(getClientPathName(m_selectedClientID));
        m_waitdlg.close.attach(&this.onChildClosed);
        m_waitdlg.appendLine("Получение параметров клиента");
        m_waitdlg.showDialog();
        
        sendMessageNonBlocking(
            makeHeader(0, NetApp.instance.coreService.selfID, CoreMessageType.QUERY_CLIENT_CONFIGS),
            makeUintPacket(m_selectedClientID)
        );
    }
    
    void processServiceVersion(ISender sender, HeaderPacket header, ubyte[] data)
    {
        assert(m_waitdlg && m_selectedExtension);
        auto p = ServiceVersionResultPacket(data);
        
        final switch(cast(int)p.type)
        {
        case ServiceResult.CREATE_SUCCESSFULLY:
            string ver = p.ver;
            m_waitdlg.appendLine("Создание локального сервиса");
            auto service = m_selectedExtension.createService(0, header.senders ~ p.id, 
                NetApp.instance.coreService, m_selectedConfigs, ver);
            
            if(service)
            {
                NetApp.instance.coreService.registerExtensionService(m_selectedExtension, cast(BaseService)service);
                auto gs = cast(IGuiService)service;
                
                if(gs)
                {
                    auto form = gs.createClientForm();
                    if(form)
                    {
                        m_waitdlg.appendLine("Окно расширения создано");
                        m_waitdlg.hide();                        
                        
                        form.close.attach(&this.onChildClosed);
                        form.text = m_selectedExtension.info ~ " - " ~ getClientPathName(m_selectedClientID);
                        form.show();
                    }
                    else
                    {
                        m_waitdlg.appendLine("Не удалось создать окно для расширения");
                    }
                }
            }
            else
            {
                m_waitdlg.appendLine("Не удалось создать сервис");
            }
            break;
            
        case ServiceResult.NAME_NOT_FOUND:
            m_waitdlg.appendLine("Расширение не поддерживается: " ~ m_selectedExtension.name);
            break;
            
        case ServiceResult.VERSION_INCOMPATIBLE:
            m_waitdlg.appendLine(format("Несовместимые версии расширения '%s' локальная %s и удаленная %s",
                m_selectedExtension.name, m_selectedExtension.ver, p.ver));
            break;
        }
        m_selectedExtension = null;
    }

private:
	TreeView m_groupTree;
	ListView m_clientList;

    MenuBar m_mainMenu;
    ContextMenu m_clientContextMenu;
    MenuItem m_servicesMenu;
    MenuItem m_deleteMenu;
    MenuItem m_propertyMenu;

    struct ClientInfo
    {
        TreeClientPacket data;
        uint parentID;
        ListViewItem item;
    }

    struct GroupInfo
    {
        TreeGroupPacket data;
        uint parentID;
        uint[] subgroups, clients;
        bool loaded;
        TreeNode node;
    }

    ClientInfo[uint] m_clients;
    GroupInfo[uint] m_groups;
    uint[TreeNode] m_nodes;
    uint m_selectedGroupID;
    uint m_selectedClientID;
    IExtension[MenuItem] m_menuToService;
    
    OpenExtensionForm m_waitdlg;
    IGuiExtension m_selectedExtension;
    string m_selectedConfigs;

    void onExit(MenuItem sender, EventArgs e)
	{
		exitEventLoop();
	}

	void onAbout()
	{
		MsgBox.show("About", "Net Admin system");
	}

	void onAbout(MenuItem sender, EventArgs e)
	{
		onAbout();
	}

	void onAbout(ToolButton sender, EventArgs e)
	{
		onAbout();
	}
    
    void onChildClosed(Control control, EventArgs e)
    {
        enabled = true;
        control.dispose();
    }

    void onTreeItemSelected(Control control, TreeNodeChangedEventArgs args)
    {
		uint* gid = args.newItem in m_nodes;
        assert(gid);

        m_selectedGroupID = *gid;
        GroupInfo* g = m_selectedGroupID in m_groups;
        assert(g);

        if(g.loaded)
        {
            updateList(g);
        }
        else 
        {
            sendMessageNonBlocking(
				makeHeader(0, NetApp.instance.coreService.selfID, CoreMessageType.QUERY_CLIENTS_TREE),
                makeUintPacket(g.data.id)
			);
        }

        m_selectedClientID = uint.max;
        updateMenu();
    }

    void onListItemSelected(Control control, EventArgs args)
    {
        GroupInfo* g = m_selectedGroupID in m_groups;
        assert(g);

        m_selectedClientID = g.clients[m_clientList.selectedItem.index];
        updateMenu();
    }

    void onProperty(MenuItem sender, EventArgs e)
	{
        if(m_selectedClientID == uint.max) return;

        sendMessageNonBlocking(
			makeHeader(0, NetApp.instance.coreService.selfID, CoreMessageType.QUERY_CLIENT_CONFIGS),
            makeUintPacket(m_selectedClientID)
		);
    }

    void updateMenu()
    {
        bool clientSelected = m_selectedClientID != uint.max;        
        m_deleteMenu.enabled = clientSelected;
        m_propertyMenu.enabled = clientSelected;

		if(clientSelected)
		{
			ClientInfo* c = m_selectedClientID in m_clients;
			bool enableServices = (c.data.info.type == ClientType.ACTOR && c.data.online);
			auto items = m_clientContextMenu.items;
			int i = 0;

			m_servicesMenu.enabled = enableServices;

			for(; i < items.length; i++)
				if(items[i].style == MenuStyle.separator)
					break;

			for(++i; i < items.length; i++)
				items[i].enabled = enableServices;
		}
		else
		{
			m_servicesMenu.enabled = false; 
		}
    }

    void updateTreeNode(GroupInfo *g)
    {
        assert(g); 

        foreach(id; g.subgroups)
        {
            GroupInfo *p = id in m_groups;
			assert(p);
            p.node = g.node.addNode(p.data.name, ICON_FOLDER, NodeInsertMode.tail);
            m_nodes[p.node] = id;
        }
        g.node.expand();        
    }

    void updateList(GroupInfo *g)
    {
        assert(g);
        m_clientList.clear();

        foreach(id; g.clients)
        {
            ClientInfo *c = id in m_clients;
			assert(c);
            
            int ii;
            final switch(cast(ClientType)c.data.info.type)
            {
            case ClientType.ACTOR: ii = c.data.online ? ICON_STATION_ON : ICON_STATION_OFF; break;
            case ClientType.ADMIN: ii = c.data.online ? ICON_ADMIN_ON : ICON_ADMIN_OFF; break;
            case ClientType.PROXY: ii = c.data.online ? ICON_SERVER_ON : ICON_SERVER_OFF; break;
            }
            c.item = m_clientList.addItem(c.data.name, ii);
			c.item.addSubItem(c.data.info.name);
			c.item.addSubItem(c.data.info.domain);
			c.item.addSubItem(c.data.info.ip);
			c.item.addSubItem(c.data.info.os);
			c.item.addSubItem(c.data.lastVisit);
        }

		m_clientList.redraw();
    }

	string getClientPathName(uint clientID)
	{
		ClientInfo* c = clientID in m_clients;
		if(!c) return "";

		string res = c.data.name;
		uint gid = c.parentID;

		while(gid)
		{
			GroupInfo* g = gid in m_groups;
			res = g.data.name ~ "\\" ~ res;
			gid = g.parentID;
		}

		return "\\" ~ res;
	}
}
