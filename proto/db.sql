CREATE TABLE "clients" (
    "id" INTEGER PRIMARY KEY NOT NULL,    
    "name" TEXT,
    "comp" TEXT,
    "domain" TEXT,
    "type" INTEGER,
    "os" TEXT,
    "ip" TEXT,
    "parent_id" INTEGER DEFAULT(0),
    "last_visit" TEXT
);

CREATE TABLE "groups" (
    "id" INTEGER PRIMARY KEY NOT NULL,
    "parent_id" INTEGER DEFAULT(0),
    "name" TEXT
);

CREATE TABLE "extensions" (
    "client_id" INTEGER NOT NULL,
    "extension_name" TEXT,
    "config" TEXT
);