module netapp;

import vibe.data.json;
import vibe.core.core;

import defs;
import service;

class NetApp
{
	static @property NetApp instance()
	{
		if(!m_app)
			m_app = new NetApp;
		return m_app;
	}

	@property ISender sender()
	{
		return m_sender;
	}

	@property CoreService coreService()
	{
		return m_service;
	}

	@property string configName()
	{
		return m_configName;
	}

	@property Json config()
	{
		return m_config;
	}

	void init(string configName, ITransport sender, CoreService service)
	{
		m_configName = configName;
		m_sender = sender;
		m_service = service;
		m_config = parseJsonString(readFileUTF8(configName));
		service.loadConfig(m_config);
	}

	int run()
	{
		runTask(&m_sender.run, m_config.transport[0].tcp.toString());
		return runEventLoop();
	}

private:
	static NetApp m_app;
	string m_configName;
	Json m_config;
	ITransport m_sender;
	CoreService m_service;

	this() 
	{
	}
}
