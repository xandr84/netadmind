module tcp;

import vibe.core.core;
import vibe.core.net;
import vibe.data.json;
import vibe.core.log;
import std.string;
import core.time;

import defs;
import utils;
import service;
import netapp;

class BaseTcpClient : ITransport
{
    this()
    {
        m_mutex = new TaskMutex();
    }
    
	override @property bool connected()
	{
		return m_conn.connected;
	}

    override void close()
    {
        m_closed = true;
    }

    override void sendMessage(HeaderPacket header, ubyte[] data)
    {
        ubyte[] head = header.serialize();

        logDebug("Sending message to %s from %s, code %s, data %d bytes",
                 uintsToStr(header.receivers), uintsToStr(header.senders), 
                 coreMsgCodeToStr(header), data.length);

        try
        {
            auto lock = ScopedMutexLock(m_mutex);
            
            uint len = head.length;
            m_conn.write((cast(ubyte*)&len)[0..len.sizeof]);
            m_conn.write(head);        

            len = data.length;
            m_conn.write((cast(ubyte*)&len)[0..len.sizeof]);

            if(len > 0)
                m_conn.write(data);
        }
        catch(Exception e)
        {
            close();
            logError("Exception (%s:%d): %s", e.file, e.line, e.msg);
        }
    }

    override void run(string configStr)
    {
        uint len; 
        ubyte[] buf;

        m_conn.readTimeout = PING_TIMEOUT;

        NetApp.instance.coreService.onConnect(0, this);

        try
        {
            while(!m_closed)
            {
                readUint(len);

                if(len >= MAX_HEADER_PACKET_SIZE)
                {
                    sendError(this, makeHeader(0, 0, CoreMessageType.ERROR), CoreErrorCode.MAX_HEADER_LENGTH_EXCEED,
                        format("Header length %d exceed max %d", len, MAX_HEADER_PACKET_SIZE));
                    close();
                    break;
                }
                else if(len > buf.length)
                {
                    buf = new ubyte[len];
                }
            
                readWithTimeout(buf[0..len]);

                auto header = HeaderPacket(buf[0..len]);

                readUint(len);

                if(len >= MAX_DATA_PACKET_SIZE)
                {
                    sendError(this, makeHeader(0, 0, CoreMessageType.ERROR), CoreErrorCode.MAX_DATA_LENGTH_EXCEED,
                              format("Data length %d exceed max %d", len, MAX_DATA_PACKET_SIZE));
                    close();
                    break;
                }
                else if(len > buf.length)
                {
                    buf = new ubyte[len];
                }

                if(len > 0)
                {
                    readWithTimeout(buf[0..len]);
                }

                logDebug("Received message to %s from %s, code %s, data %d bytes",
                    uintsToStr(header.receivers), uintsToStr(header.senders), 
                    coreMsgCodeToStr(header), len);

                NetApp.instance.coreService.processMessage(this, header, 0, buf[0..len]);            
            }
        }
        catch(Exception e)
        { 
            logDebug("Exception (%s:%d): %s", e.file, e.line, e.msg);
            logInfo("Client disconnected %s", m_conn.remoteAddress);            
        } 

        NetApp.instance.coreService.onDisconnect(0, this);
    }

protected:
    TCPConnection m_conn;
    bool m_closed;
    TaskMutex m_mutex;

    void readWithTimeout(ubyte[] buf)
    {
        while(!m_closed)
        {
            if(m_conn.waitForData(m_conn.readTimeout))
            {
                m_conn.read(buf);
                return;
            }
        }
        throw new Exception("Read timeout");
    }

    void readUint(ref uint val)
    {
        readWithTimeout((cast(ubyte*)&val)[0..val.sizeof]);
    }
}

class TcpClient : BaseTcpClient
{
    override void run(string configStr)
    {
        string host = "localhost";
        ushort port = 6800;
        uint sleepSec = 60;
        
        try {
            Json config = parseJsonString(configStr);
            host = config.host.to!string;
            port = config.port.to!ushort;
            sleepSec = config.sleep.to!uint;
        }
        catch(Exception e)
        {
            logError(e.msg);
        }        

        while(true)
        {
            m_conn = null;
            logInfo("Connecting to server %s:%d", host, port);

            try 
            {
                m_conn = connectTCP(host, port);
                m_closed = false;

                ubyte[4] buf;
                m_conn.read(buf);
                char[] proto = (cast(char*)buf)[0..4];
                if(!std.algorithm.equal(proto, PROTOCOL_NAME))
                {
                    logError("Received unsupported protocol '%s'", proto[0..4]);
                    m_conn.close();
                    return;
                }

                super.run(configStr);                
            }
            catch(Exception e)
            {
                logDebug("Exception (%s:%d): %s", e.file, e.line, e.msg);
            }

			logInfo("Wait %s s before reconnect", sleepSec);
			sleep(seconds(sleepSec)); 
        }
    }
}

class TcpInternalClient : BaseTcpClient
{
    this(TCPConnection conn)
    {
        m_conn = conn;
    }

    override void run(string configStr)
    {
        m_conn.write(PROTOCOL_NAME);
        super.run(configStr);
    }
}

class TcpServer : ITransport
{
	override @property bool connected()
	{
		return false;
	}

    override void close()
    {
    }

    override void run(string configStr)
    {
        string host = "localhost";
        ushort port = 6800;
        
        try {
            Json config = parseJsonString(configStr);
            host = config.host.to!string;
            port = config.port.to!ushort;
        }
        catch(Exception e)
        {
            logError(e.msg);
        }        
        
        logInfo("Start listen on %s:%d", host, port);
        listenTCP(port, &onConnect, host);
    }

    void onConnect(TCPConnection conn)
    {
        logInfo("Connected new client from %s", conn.remoteAddress);
        auto client = new TcpInternalClient(conn);
        client.run("undefined");
    }

    override void sendMessage(HeaderPacket header, ubyte[] data)
    {
        assert(0, "TcpServer can not send message");
    }
}

class TcpTransportFactory : ITransportFactory
{
    ITransport createTransport(string name)
    {
        if(name == "tcpclient")
            return new TcpClient;

        if(name == "tcpserver")
            return new TcpServer;

        return null;
    }
}