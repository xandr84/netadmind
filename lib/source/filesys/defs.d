module filesys.defs;

import dproto.dproto;
import vibe.core.core;
import std.file;
import std.path;
import std.stream;

import defs;
import utils;
import service;

mixin ProtocolBuffer!"filesys.proto";

enum FileSystemMessage
{
    ERROR = 0,                  /* actor to admin: UintStrPacket
    */
    
    QUERY_FOLDER_CONTENT = 1,   /* admin to actor: UintStrPacket
        Запрос на перечень содержимого папки, указанной в поле text.
        code - для будущего использования
    */
    
    FOLDER_CONTENT_INFO = 2,    /* actor to admin: compressed FolderContentPacket
        Ответ в сжатом виде с перечнем подпапок и файлов
    */
    
    OPEN_FILE = 3,              /* admin to actor: UintStrPacket
        Запрос на открытие файла. 
        text - полный путь к файлу
        code - значение vibe.core.file.FileMode
        В случае неудачи возвращается ERROR с кодом ошибки и описанием в UintStrPacket
    */
    
    OPENED_FILE_INFO = 4,       /* actor to admin: FileInfoPacket
        Сообщает об успешности открытия файла.
        name - полный путь
    */
    
    READ_FILE = 5,              /* admin to actor: FileDataRequestPacket
        Запрос на чтение блока из файла с позиции offset и размером size
        и отправку в виде нескольких сообщений оптимального размера.
        Данные переслаются с сообщением FILE_DATA
    */
    
    WRITE_FILE = 6,
    
    APPEND_FILE = 7,
    
    FILE_DATA = 8,              /* client to client: bytes
        Запрошенный блок данных в сыром виде без сериализации.
    */  

    END_OF_FILE = 9,
    
    PAUSE_TRASFER = 10,
    
    RESUME_TRANSFER = 11,
    
    STOP_TRANSFER = 12,
    
    CLOSE_FILE = 13,
}

enum FileSystemError
{
	PATH_NOT_EXISTS = 1,
}

enum FolderType				//Partial equal with result of GetDriveType
{
	DRIVE_UNKNOWN = 0,		//The drive type cannot be determined.
	DRIVE_NO_ROOT_DIR = 1,	//The root path is invalid; for example, there is no volume mounted at the specified path.
	DRIVE_REMOVABLE = 2,	//The drive has removable media; for example, a floppy drive, thumb drive, or flash card reader.
	DRIVE_FIXED = 3,		//The drive has fixed media; for example, a hard disk drive or flash drive.
	DRIVE_REMOTE = 4,		//The drive is a remote (network) drive.
	DRIVE_CDROM = 5,		//The drive is a CD-ROM drive.
	DRIVE_RAMDISK = 6,		//The drive is a RAM disk.
	DIRECTORY = 10,
	SYMLINK = 11
}

enum {
	MAX_FILE_DATA_PACKET = 100000
}

version (Windows)
{
    import core.sys.windows.windows;	

    extern (Windows):
    nothrow:
    
    enum SEM_FAILCRITICALERRORS = 0x0001;
    UINT SetErrorMode(UINT uMode);

    DWORD GetLogicalDriveStringsA(DWORD nBufferLength, LPTSTR lpBuffer);

    UINT GetDriveTypeA(LPCTSTR lpRootPathName);

    BOOL GetDiskFreeSpaceExA(LPCTSTR lpDirectoryName, PULARGE_INTEGER lpFreeBytesAvailable,
        PULARGE_INTEGER lpTotalNumberOfBytes, PULARGE_INTEGER lpTotalNumberOfFreeBytes);
}

abstract class FileSysService : BaseService
{
    this(uint id, IService parent = null)
    {
        super(id, parent);
    }

    void enumDrives(ref FolderContentPacket fcp)
    {
        char[512] buf;
        uint len = GetLogicalDriveStringsA(buf.length, buf.ptr);

        for(uint i = 0, j = 0; i < len; i++)
        {
            if(buf[i] == 0)
            {
                ULARGE_INTEGER freeBytes, total, totalFree;
                FolderInfoPacket fip;

                fip.type = GetDriveTypeA(buf.ptr + j);
                fip.name = (cast(string)buf[j..i]).dup;
                GetDiskFreeSpaceExA(buf.ptr + j, &freeBytes, &total, &totalFree);
                fip.capacity = total.QuadPart;
                fip.free = totalFree.QuadPart;
                fcp.folders ~= fip;
                j = i+1;
            }
        }        
    }

    void enumFolders(string path, ref FolderContentPacket fcp)
    {
        foreach (DirEntry e; dirEntries(path, SpanMode.shallow))
        {
            if(e.isFile)
            {
                FileInfoPacket p;
                p.name = baseName(e.name);
                p.size = e.size;
                p.timeLastModified = e.timeLastModified.stdTime;
                fcp.files ~= p;
            }
            else
            {
                FolderInfoPacket p;
                p.name = baseName(e.name);
                p.type = e.isSymlink ? FolderType.SYMLINK : FolderType.DIRECTORY;
                p.timeLastModified = e.timeLastModified.stdTime;
                fcp.folders ~= p;
            }
        }        
    }

    void localCopyFile(string fromPath, string toPath, bool withMove)
    {
        
    }
}