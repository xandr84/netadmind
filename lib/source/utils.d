module utils;

import vibe.data.json;
import vibe.core.log;
import std.datetime;
import std.string;
import std.array;
import std.algorithm.iteration;
import std.conv;

import defs;

HeaderPacket makeHeader(uint recID, uint sendID, uint code)
{
    HeaderPacket h;
    h.receivers = [recID];
    h.senders = [sendID];
    h.code = code;
    return h;
}

HeaderPacket makeHeader(uint[] receivers, uint[] senders, uint code)
{
    HeaderPacket h;
    h.receivers = receivers;
    h.senders = senders;
    h.code = code;
    return h;
}

ubyte[] makeUintPacket(uint value)
{
	UintPacket p;
	p.value = value;
	return p.serialize();
}

void sendAnswer(ISender sender, HeaderPacket header, uint code, ubyte[] data)
{
	sender.sendMessage(makeHeader(header.senders, header.receivers, code), data);
}

void sendError(ISender sender, HeaderPacket header, uint errorCode, string text, uint code = CoreMessageType.ERROR)
{
	sendAnswer(sender, header, code, makeError(errorCode, text).serialize());
    logError("Error: %s", text);
}

ErrorPacket makeError(uint code, string text)
{
    ErrorPacket e;
    e.code = code;
    e.text = text;
    return e;
}

string timeToSqlStr(SysTime time)
{
    return format("%04d-%02d-%02d %02d:%02d:%02d", 
                  time.year, time.month, time.day, 
                  time.hour, time.minute, time.second);
}

Json optValue(Json json, string key, Json defValue = Json.undefined)
{
    try {
        return json[key]; 
    }
    catch(Exception e) {}
    return defValue;
}

T optValue(T)(Json json, string key, T defValue)
{
    try {
        return json[key].opt!T(defValue); 
    }
    catch(Exception e) {}
    return defValue;
}

string ulongToSpacedStr(ulong value)
{
    uint[] items;
    
    while(value > 0)
    {
        items ~= value % 1000;
        value /= 1000;
    }
    
    std.algorithm.reverse(items);
    
    auto a = appender!string();
    foreach(c; items)
    {
        a.put(std.conv.text(c));
        a.put(' ');
    }
    
    return a.data;
}

string uintsToStr(uint[] a)
{
    return join(map!(to!string)(a), '.');
}

string coreMsgCodeToStr(HeaderPacket header)
{
    if(header.receivers.length <= 1 && header.senders.length <= 1)
        return to!string(cast(CoreMessageType)header.code);
    return to!string(header.code);
}
