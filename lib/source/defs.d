module defs;

import vibe.core.stream;
import vibe.data.json;
import std.string;
import core.time;

import dproto.dproto;

mixin ProtocolBuffer!"core.proto";
alias UintStrPacket ErrorPacket;

const PROTOCOL_NAME = "NaDM";
const MAX_HEADER_PACKET_SIZE = 1024;
const MAX_DATA_PACKET_SIZE = 1024*1024;
auto PING_TIMEOUT = seconds(63);

enum CoreMessageType
{
    /*  all = actor, admin, proxy 
        client = actor, admin и proxy (дочерний)
    */    
        
    ERROR = 0,                  /* all to all: ErrorPacket
        Высылается в случае ошибки с кодом и текстовым описанием как результат любого из запросов
    */
    
    CLIENT_INFO = 2,            /* client to proxy: ClientInfoPacket
        Первое сообщение от клиента с информацией о себе. Другие сообщения не принимаются до него.
        В случае успешной регистрации (совпал переданный ClientID или создан новый) отправляется
        сообщение CLIENT_REG
    */
    
    CLIENT_REG = 3,             /* proxy to client: UintPacket with new ClientID
        Код клиента, который должен использоваться в последующих сообщениях.
        Если в сообщении CLIENT_INFO какой-то код передавался, то здесь может прийти новый.
    */
    
    QUERY_CLIENTS_TREE = 4,     /* admin to proxy: UintPacket with tree group ID (zero for root)
        Запрос содержимого иерархии клиентов. На вход подается ID группы (либо нуль для корневой).
        В ответ отправляется CLIENTS_TREE_GROUP_DATA
    */
    
    CLIENTS_TREE_GROUP_DATA = 5,/* proxy to admin: ClientsTreeGroupPacket
        Ответ с перечнем подгрупп и параметров клиентов в указанной ранее группе
    */
    
    CLIENT_ONLINE = 6,          /* proxy to admin & proxy: UintPacket with client ID
        Уведомление о том, что клиент с указанным ID подключился к proxy
    */
    
    CLIENT_OFFLINE = 7,         /* proxy to admin & proxy: UintPacket with client ID
        Уведомление о том, что клиент с указанным ID отключился от proxy
    */
    
    PING = 8,                   //proxy to clients: empty data
    PONG = 9,                   //clients to proxy: empty data
    
    QUERY_CLIENT_CONFIGS = 10,  //admin to proxy: UintPacket with client ID    
    CLIENT_CONFIGS = 11,        //proxy to admin: ClientConfigsPacket
    
    NEGOTIATE_SERVICE_VERSION = 12, /* admin to actor: ServiceVersionPacket
        Запрос на согласование версий расширений. Выполняется перед созданием окна расширения в Admin.
        В ответ отправляется SERVICE_VERSION_RESULT
    */
    
    SERVICE_VERSION_RESULT = 13,    /* actor to admin: ServiceVersionResultPacket
        Ответ на согласование версий с повтором name из запроса. 
        В случае успеха type==CREATE_SUCCESSFULLY и id==<код сервиса>.
        Если такого расширения нет, то type==NAME_NOT_FOUND.
        Если не сошлись версии, то type==VERSION_INCOMPATIBLE, ver=<подходящая версия>, id=<код сервиса>.
        В последнем случае если ver не подходит, то нужно закрыть сервис, отправив CLOSE_SERVICE
    */
    
    CLOSE_SERVICE = 14,             /*admin to actor: UintPacket with service ID
        Сообщение об отсутствие необходимости в объекте сервиса (обычно при закрытии окна расширения в Admin)
    */
    
    QUERY_SERVICE_CONFIG = 15,
}

enum CoreErrorCode
{
    MAX_HEADER_LENGTH_EXCEED = 1,
    MAX_DATA_LENGTH_EXCEED = 2,
    UNKNOWN_MESSAGE_CODE = 3,
    UNKNOWN_SERVICE_ID = 4,
    UNKNOWN_RECEIVER_ID = 5,
    CLIENT_NOT_REGISTERED = 6
}

interface IReceiver
{
    void processMessage(ISender sender, HeaderPacket header, uint receiverIndex, ubyte[] data);
}

interface ISender
{
    @property bool connected();
    void sendMessage(HeaderPacket header, ubyte[] data);
    void close();
}

interface ITransport : ISender
{
    void run(string configStr);     //string instead Json due to vibe.d error
}

interface ITransportFactory
{
    ITransport createTransport(string name);
}

interface IService : IReceiver
{
    @property uint selfID();
    @property uint[] fullID();

    void onConnect(uint clientID, ISender sender);
    void onDisconnect(uint clientID, ISender sender);
}

interface IExtension
{
    @property string name();
    @property string ver();
    @property string info();
    
    IService createService(uint id, uint[] clientIDs, IService parent, string config, ref string ver);
}

