module service;

import vibe.core.core;
import vibe.data.json;
import vibe.core.log;
import vibe.core.file;
import std.string;
import std.datetime;

import defs;
import utils;
import netapp;

alias ReceiverFunc = void delegate(ISender sender, HeaderPacket header, ubyte[] data);

abstract class BaseService : IService
{
    this(uint id = 0, IService parent = null)
    {
        m_parent = parent;
        m_fullID = parent ? parent.fullID ~ [id] : [id];        
        registerReceiverFunc(CoreMessageType.ERROR, &processError);
    }
    
    override @property uint selfID()
    {
        return m_fullID[$-1];
    }

    @property void selfID(uint id) 
    {
        m_fullID[$-1] = id;
    }
    
    override @property uint[] fullID()
    {
        return m_fullID;
    }
    
    @property IService[] services()
    {
        return m_serviceMapper.values;
    }

    void registerReceiverFunc(uint code, ReceiverFunc func)
    {
        if(func)
            m_msgCodeMapper[code] = func;
    }
    
    void unregisterReceiverFunc(uint code)
    {
        m_msgCodeMapper.remove(code);
    }
    
    void registerSubService(IService service)
    {
        if(service)
            m_serviceMapper[service.selfID] = service;
    }
    
    void unregisterSubService(IService service)
    {
        if(service)
            m_serviceMapper.remove(service.selfID);
    }

    override void onConnect(uint clientID, ISender sender)
    {
        foreach(s; m_serviceMapper.byValue)
            s.onConnect(clientID, sender);
    }

	override void onDisconnect(uint clientID, ISender sender)
    {
        foreach(s; m_serviceMapper.byValue)
            s.onDisconnect(clientID, sender);
    }

protected:
    uint[] m_fullID;
    IService m_parent;
    IService[uint] m_serviceMapper;
    ReceiverFunc[uint] m_msgCodeMapper;

    override void processMessage(ISender sender, HeaderPacket header, uint receiverIndex, ubyte[] data)
    {
        if(header.receivers.length > receiverIndex && 
            (header.receivers[receiverIndex] == selfID || header.receivers[receiverIndex] == 0))
        {
            ++receiverIndex;
        }
        
        if(header.receivers.length <= receiverIndex) //message for me
        {
            if(header.code in m_msgCodeMapper) 
                processMessageCode(sender, header, data);
            else processUnknownMessageCode(sender, header, data);
        }
        else //for subService
        {
            if(header.receivers[receiverIndex] in m_serviceMapper) 
                processServiceMessage(sender, header, receiverIndex, data);            
            else processUnknownServiceMessage(sender, header, receiverIndex, data);
        }
	}
    
    void processMessageCode(ISender sender, HeaderPacket header, ubyte[] data)
    {
        m_msgCodeMapper[header.code](sender, header, data);
    }
    
    void processUnknownMessageCode(ISender sender, HeaderPacket header, ubyte[] data)
    {
        sendError(sender, header, CoreErrorCode.UNKNOWN_MESSAGE_CODE,
            format("Unknown message code %d from sender %s", header.code, uintsToStr(header.senders))
        );
    }
    
    void processServiceMessage(ISender sender, HeaderPacket header, uint receiverIndex, ubyte[] data)
    {
        m_serviceMapper[header.receivers[receiverIndex]].processMessage(sender, header, receiverIndex+1, data);
    }
    
    void processUnknownServiceMessage(ISender sender, HeaderPacket header, uint receiverIndex, ubyte[] data)
    {
        sendError(sender, header, CoreErrorCode.UNKNOWN_SERVICE_ID,
            format("Unknown receiver service id %s", uintsToStr(header.receivers[0..receiverIndex]))
        );
    }

    void processError(ISender sender, HeaderPacket header, ubyte[] data)
    {
        auto error = ErrorPacket(data);
        logError("Received error %d: %s", error.code, error.text);
    }
}

abstract class CoreService : BaseService
{
	this(ClientType type, uint id = 0, IService parent = null)
	{
        super(id, parent);
		m_type = type;
        m_nextServiceID = 1;
        m_pingTimer = createTimer(&pingServer);
        
        registerReceiverFunc(CoreMessageType.PING, &processPing);
        registerReceiverFunc(CoreMessageType.PONG, &processPong);
        registerReceiverFunc(CoreMessageType.CLIENT_REG, &processClientReg);
        registerReceiverFunc(CoreMessageType.CLIENT_ONLINE, &processClientStatus);
        registerReceiverFunc(CoreMessageType.CLIENT_OFFLINE, &processClientStatus);
        registerReceiverFunc(CoreMessageType.NEGOTIATE_SERVICE_VERSION, &processNegServiceVer);
	}

    @property ClientType type() 
    {
        return m_type;
    }

    @property Json config()
    {
        return m_config;
    }
    
    @property IExtension[] extensions()
    {
        IExtension[] res;
        foreach(e; m_extensions)
            res ~= e.ext;
        return res;
    }

    void loadConfig(Json config)
	{
        m_config = config;
        selfID = config.id.get!uint();
	}
    
    void registerExtension(IExtension ext)
    {
        if(ext) m_extensions ~= ExtensionInfo(ext);
    }
    
    void unregisterExtension(IExtension ext)
    {
        if(ext is null) return;
        
        for(int i = 0; i < m_extensions.length; i++)        
            if(ext == m_extensions[i].ext)
            {
                foreach(id; m_extensions[i].serviceIDs)
                    unregisterSubService(m_serviceMapper[id]);
                std.algorithm.mutation.remove(m_extensions, i);
                break;
            }        
    }
    
    void registerExtensionService(IExtension ext, BaseService service)
    {
        if(service.selfID == 0)
            service.selfID = m_nextServiceID++;
            
        registerSubService(service);
        
        foreach(einfo; m_extensions)
            if(einfo.ext == ext)
            {
                einfo.serviceIDs ~= service.selfID;
                break;
            }
    }

    override void onConnect(uint clientID, ISender sender)
	{
        if(clientID == selfID || clientID == 0)
            m_pingTimer.rearm(PING_TIMEOUT, true);
            
        super.onConnect(clientID, sender);
    }

    override void onDisconnect(uint clientID, ISender sender)
	{
        if(clientID == selfID || clientID == 0)
            m_pingTimer.stop();
            
        super.onDisconnect(clientID, sender);
	} 

    override void processMessage(ISender sender, HeaderPacket header, uint receiverIndex, ubyte[] data)
	{
        m_lastMessageTime = Clock.currTime();
        super.processMessage(sender, header, receiverIndex, data);
	}

protected:
    struct ExtensionInfo
    {
        IExtension ext;
        uint[] serviceIDs;
    }
    
    ClientType m_type;
    uint m_nextServiceID;
    Json m_config;
    Timer m_pingTimer;
    SysTime m_lastMessageTime;
	ExtensionInfo[] m_extensions;

    void pingServer()
    {
        SysTime time = Clock.currTime();

        if(time - m_lastMessageTime >= PING_TIMEOUT)
        {
            NetApp.instance.sender.sendMessage(makeHeader(0, selfID, CoreMessageType.PING), null);
        }
    }    

    void onClientReg(uint clientID, ISender sender)
    {
        if(selfID != clientID)
        {
            selfID = clientID;
            m_config.id = clientID;

            logInfo("Received new id %d", clientID);

            writeFileUTF8(Path(NetApp.instance.configName), m_config.toPrettyString);
        }
    }

    void processPing(ISender sender, HeaderPacket header, ubyte[] data)
    {
        sendAnswer(sender, header, CoreMessageType.PONG, null);
    }

    void processPong(ISender sender, HeaderPacket header, ubyte[] data)
    {
        //nothing
    }

    void processClientReg(ISender sender, HeaderPacket header, ubyte[] data)
    {
        auto p = UintPacket(data);
        onClientReg(p.value, sender);
    }

    void processClientStatus(ISender sender, HeaderPacket header, ubyte[] data)
    {
        auto p = UintPacket(data);

        logInfo("Client %d change status to %s", p.value, 
                header.code == CoreMessageType.CLIENT_ONLINE ? "online" : "offline");

        if(header.code == CoreMessageType.CLIENT_ONLINE)
            onConnect(p.value, sender);
        else onDisconnect(p.value, sender);
    }
    
    void processNegServiceVer(ISender sender, HeaderPacket header, ubyte[] data)
    {
        auto p = ServiceVersionPacket(data);
        ServiceVersionResultPacket r;
        
        r.type = ServiceResult.NAME_NOT_FOUND;
        r.name = p.name;
        
        foreach(einfo; m_extensions)
            if(einfo.ext.name == p.name)
            {
                string ver = p.ver;
                IService service = einfo.ext.createService(m_nextServiceID, header.senders, this, p.config, ver);
                r.ver = ver;
                
                if(service)
                {
                    ++m_nextServiceID;
                    einfo.serviceIDs ~= service.selfID;
                    registerSubService(service);                    
                    
                    r.type = ServiceResult.CREATE_SUCCESSFULLY;
                    r.id = service.selfID;
                }
                else r.type = ServiceResult.VERSION_INCOMPATIBLE;                
                break;
            }
        
        sendAnswer(sender, header, CoreMessageType.SERVICE_VERSION_RESULT, r.serialize());
    }
}

class ClientCoreService : CoreService
{
    this(ClientType t, uint id = 0, IService parent = null)
    {
        super(t, id, parent);        
    }

	override void onConnect(uint clientID, ISender sender)
	{
        super.onConnect(clientID, sender);

        if(clientID == 0)
        {
		    ClientInfoPacket cip;
		    cip.type = this.type;

		    version(Windows)
		    {
			    import core.sys.windows.windows;

			    char[256] name; 
			    uint len = name.length;
			    GetComputerNameA(name.ptr, &len);
			    cip.name = cast(string)name[0..len];
		    }

		    sender.sendMessage(
			    makeHeader(0, this.selfID, CoreMessageType.CLIENT_INFO),
			    cip.serialize()
		    );
        }
	}   
}