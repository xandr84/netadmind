PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE "clients" (
    "id" INTEGER PRIMARY KEY NOT NULL,    
    "name" TEXT,
    "comp" TEXT,
    "domain" TEXT,
    "type" INTEGER,
    "os" TEXT,
    "ip" TEXT,
    "parent_id" INTEGER DEFAULT(0),
    "last_visit" TEXT
);

CREATE TABLE "groups" (
    "id" INTEGER PRIMARY KEY NOT NULL,
    "parent_id" INTEGER DEFAULT(0),
    "name" TEXT
);

CREATE TABLE "services" (
    "client_id" INTEGER NOT NULL,
    "service_id" INTEGER NOT NULL,
    "config" TEXT
);

INSERT INTO "clients" VALUES(1001,'actor','XANDR-NEW','',1,'','',0);
INSERT INTO "clients" VALUES(1002,'admin','XANDR-NEW','',2,'','',0);
INSERT INTO "clients" VALUES(1003,'Директор','BOSS','VOL',1,'WinXP',NULL,1);
INSERT INTO "clients" VALUES(1004,'Сервер','SERVER','VOL',NULL,NULL,NULL,1);
INSERT INTO "clients" VALUES(1005,'Сервер','SERVER',NULL,NULL,NULL,NULL,2);

INSERT INTO "groups" VALUES(1,'Волчиха',0);
INSERT INTO "groups" VALUES(2,'Михайловка',0);
COMMIT;
