import std.stdio;

import vibe.data.json;
import vibe.core.file;
import vibe.core.core;
import vibe.core.log;

import defs;
import netapp;
import tcp;
import coreservice;

int main()
{
    setLogLevel(LogLevel.debug_);

	auto logger = new HTMLLogger("proxy.log.html");
	logger.minLogLevel = LogLevel.debugV;
	registerLogger(cast(shared)logger);

    NetApp.instance.init("proxy.config", new TcpServer, new ServerCoreService);
    return NetApp.instance.run();
}

