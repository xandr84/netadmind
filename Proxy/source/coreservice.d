module coreservice;

import vibe.core.core;
import vibe.core.log;
import vibe.data.json;
import vibe.core.file;
import std.string;
import std.datetime;
import d2sqlite3;

import defs;
import service;
import utils;

class ServerCoreService : CoreService
{
    this()
    {
        super(ClientType.PROXY);
        m_nextServiceID = 1001;
        
        registerReceiverFunc(CoreMessageType.CLIENT_INFO, &processClientInfo);        
        registerReceiverFunc(CoreMessageType.QUERY_CLIENTS_TREE, &processQueryClientsTree);
        registerReceiverFunc(CoreMessageType.QUERY_CLIENT_CONFIGS, &processQueryClientConfigs);

        setTimer(PING_TIMEOUT, &pingAll, true);
    }
    
    override void processMessageCode(ISender sender, HeaderPacket header, ubyte[] data)
    {
        if(header.code == CoreMessageType.CLIENT_INFO)
        {
            super.processMessageCode(sender, header, data);
        }
        else if(header.senders.length > 0 && header.senders[0] in m_serviceMapper)
        {
            auto client = cast(ClientSubService)m_serviceMapper[header.senders[0]];
            
            if(client && client.registered)
            {
                super.processMessageCode(sender, header, data);
            }
            else
            {
                sendError(sender, header, CoreErrorCode.CLIENT_NOT_REGISTERED, 
                    format("Client %d need registration", header.senders[0])
                );
            }
        }
        else
        {
            sendError(sender, header, CoreErrorCode.CLIENT_NOT_REGISTERED, 
                    "Client need registration");
        }
    }
    
    override void processUnknownServiceMessage(ISender sender, HeaderPacket header, uint receiverIndex, ubyte[] data)
    {
        sendError(sender, header, CoreErrorCode.UNKNOWN_RECEIVER_ID,
            format("Unknown receiver id %s", uintsToStr(header.receivers[0..receiverIndex]))
        );
    }

    override void processMessage(ISender sender, HeaderPacket header, uint receiverIndex, ubyte[] data)
    {
        if(header.senders.length > 0 && header.senders[0] in m_serviceMapper)
        {
            auto client = cast(ClientSubService)m_serviceMapper[header.senders[0]];
            client.lastMessageTime = Clock.currTime();
        }

        super.processMessage(sender, header, receiverIndex, data);        
    }

    void pingAll()
    {
        SysTime time = Clock.currTime();
        ClientSubService[] clientsForClose;

        foreach(id, c; m_serviceMapper)
        {
            auto client = cast(ClientSubService)c;
            if(client && time - client.lastMessageTime >= PING_TIMEOUT)
            {
                try 
                {
                    client.receiver.sendMessage(makeHeader(id, selfID, CoreMessageType.PING), null);
                }
                catch(Exception e) 
                {
                    clientsForClose ~= client;                    
                }
            }
        }

        foreach(client; clientsForClose)
        {
            logInfo("Disconnected by ping client id = %d ", client.selfID);
            client.receiver.close();
            unregisterSubService(client);
        }

        foreach(client; clientsForClose)
        {
            sendNotifyMessage(client.selfID, CoreMessageType.CLIENT_OFFLINE, 
                makeUintPacket(client.selfID));
        }
    }

    override void onConnect(uint clientID, ISender sender)
    {
        //super.onConnect(sender) disabled
    }

    override void onDisconnect(uint clientID, ISender sender)
    {
        //super.onDisconnect(sender) disabled

        foreach(id, c; m_serviceMapper)
        {
            auto client = cast(ClientSubService)c;
            if(client && client.receiver == sender)
            {
                logInfo("Disconnected client id = %d ", id);
                sendNotifyMessage(id, CoreMessageType.CLIENT_OFFLINE, makeUintPacket(id));
                unregisterSubService(c);
                break;
            }        
        }
    }

    override void loadConfig(Json config)
    {
        super.loadConfig(config);

        loadDB(config.db.to!string);
    }

    void processClientInfo(ISender sender, HeaderPacket header, ubyte[] data)
    {
        uint id = header.senders.length > 0 ? header.senders[0] : 0;
        bool registered = id == 0 ? false : isClientRegistered(id);
        auto cip = ClientInfoPacket(data);        

        if(!registered)
        {
            if(this.config.registrationAllowed.opt!bool(false))
            {
                id = m_nextServiceID++;
            }
            else
            {
                sendError(sender, header, CoreErrorCode.CLIENT_NOT_REGISTERED, 
                          format("Client %d not registered", id));
                return;
            }
        }

        sendAnswer(sender, header, CoreMessageType.CLIENT_REG, makeUintPacket(id));

        auto client = new ClientSubService(id, sender, cip.type);
        registerSubService(client);
        insertClientInfo(id, cip, registered);

        logInfo("Connected client info: id=%d, type=%d, name=%s, domain=%s",
                id, cip.type, cip.name, cip.domain);

        sendNotifyMessage(id, CoreMessageType.CLIENT_ONLINE, makeUintPacket(id));        
    }

    void processQueryClientsTree(ISender sender, HeaderPacket header, ubyte[] data)
    {
        auto p = UintPacket(data);        

        ClientsTreeGroupPacket ctg;
        uint rootID = p.value;
        ctg.rootID = p.value;        

        auto query = m_db.prepare(`SELECT id, name FROM groups WHERE parent_id = ?`);
        query.bind(1, rootID);
        auto rows = query.execute();

        foreach(r; rows)
        {
            TreeGroupPacket gp;
            gp.id = r[0].as!uint;
            gp.name = r[1].as!string;
            ctg.groups ~= gp;
        }

        query = m_db.prepare(`SELECT id, type, name, comp, domain, os, ip, last_visit 
                           FROM clients WHERE parent_id = ?`);
        query.bind(1, rootID);
        rows = query.execute();

        foreach(r; rows)
        {
            TreeClientPacket cp;
            cp.id = r[0].as!uint;
            cp.name = r[2].as!string;
            cp.online = cast(bool)(cp.id in m_serviceMapper);
            cp.info.type = cast(ClientType)r[1].as!uint;
            cp.info.name = r[3].as!string;
            cp.info.domain = r[4].as!string;
            cp.info.os = r[5].as!string;
            cp.info.ip = r[6].as!string;
            cp.lastVisit = r[7].as!string;
            ctg.clients ~= cp;            
        }

        sendAnswer(sender, header, CoreMessageType.CLIENTS_TREE_GROUP_DATA, ctg.serialize());
    }

    void processQueryClientConfigs(ISender sender, HeaderPacket header, ubyte[] data)
    {
        auto p = UintPacket(data);
        auto query = m_db.prepare(`SELECT extension_name, config FROM extensions WHERE client_id = ?`);
        query.bind(1, cast(uint)p.value);
        auto rows = query.execute();
        auto json = Json.emptyObject;
        
        foreach(r; rows)
            json[r[0].as!string] = r[1].as!string;            

        ClientConfigsPacket res;
        res.clientID = p.value;
        res.configs = json.toString();        

        sendAnswer(sender, header, CoreMessageType.CLIENT_CONFIGS, res.serialize());
    }
    

///////////////////////////////////////////////////////////////////////////////
private:
    class ClientSubService : IService
    {
        uint id;
        ISender receiver;
        ClientType type;
        bool registered;
        SysTime lastMessageTime;
        
        this(uint id, ISender receiver, ClientType type)
        {
            this.id = id;
            this.receiver = receiver;
            this.type = type;
            this.registered = true;
            this.lastMessageTime = Clock.currTime();
        }
        
        @property uint selfID() { return id; }
        @property uint[] fullID() { return [id]; }

        void onConnect(uint clientID, ISender sender) { }
        void onDisconnect(uint clientID, ISender sender) { }
        
        void processMessage(ISender sender, HeaderPacket header, uint receiverIndex, ubyte[] data)
        {
            receiver.sendMessage(header, data);
        }
    }

    //uint m_nextClientID;
    //ClientInfo[uint] m_clients;
    Database m_db;    

    void sendNotifyMessage(uint clientID, CoreMessageType code, ubyte[] data)
    {
        foreach(id, c; m_serviceMapper)
        {
            auto client = cast(ClientSubService)c;
            if(client && id != clientID /*&& (c.type == ClientType.ADMIN || c.type == ClientType.PROXY)*/)
            {
                client.receiver.sendMessage(makeHeader(id, selfID, code), data);
            }
        }
    }

    void loadDB(string path)
    {
        bool needInit = !existsFile(path);
        m_db = Database(path);

        if(needInit)
        {
            string sql = import("db.sql");
            m_db.execute(sql);
        }
        else
        {
            auto query = m_db.execute("SELECT MAX(id) FROM clients");
            uint v = query.oneValue!uint;
            m_nextServiceID = (v == 0 ? 1001 : v+1);

            if(m_nextServiceID == selfID)
                m_nextServiceID++;
        }
    }

    void insertClientInfo(uint id, ref ClientInfoPacket cip, bool update)
    {
        string updateSql = `UPDATE clients SET comp=:comp, domain=:domain, type=:type, os=:os, ip=:ip, 
            last_visit=:last WHERE id=:id`;
        string insertSql = `REPLACE INTO clients(id, comp, domain, type, os, ip, last_visit) 
            VALUES (:id, :comp, :domain, :type, :os, :ip, :last)`;

        auto query = m_db.prepare(update ? updateSql : insertSql);
        query.bind(":id", id);
        query.bind(":comp", cast(string)cip.name);
        query.bind(":domain", cast(string)cip.domain);
        query.bind(":type", cast(uint)cip.type);
        query.bind(":os", cast(string)cip.os);
        query.bind(":ip", cast(string)cip.ip);
        query.bind(":last", timeToSqlStr(Clock.currTime()));
        query.execute();

        m_db.execute(`UPDATE clients SET name = comp WHERE name is NULL`);
    }

    bool isClientRegistered(uint clientID)
    {
        auto query = m_db.prepare(`SELECT id FROM clients WHERE id = ?`);
        query.bind(1, clientID);
        auto rows = query.execute();
        return !rows.empty;
    }
}

